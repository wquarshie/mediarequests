package main

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

// PerFileResponse represents the API resultset	.
type PerFileResponse struct {
	Items []PerFile `json:"items"`
}

// PerFile represents an entry in the API resultset.
type PerFile struct {
	Referer     string `json:"referer"`
	FilePath    string `json:"file_path"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Agent       string `json:"agent"`
	Requests    int    `json:"requests"`
}

// PerFileHandler is an HTTP handler for mediarequests/per-file API requests.
type PerFileHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *PerFileHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var response = PerFileResponse{Items: make([]PerFile, 0)}

	// Parameters
	var referer = strings.ToLower(params.ByName("referer"))
	var agent = params.ByName("agent")
	var filePath = params.ByName("file-path")
	var granularity = strings.ToLower(params.ByName("granularity"))
	var start, end string

	decodedFilePath, err := url.QueryUnescape(filePath)
	if err != nil {
		s.logger.Error("Error decoding filepath: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	// Parameter validation
	if agent != "all-agents" && agent != "spider" && agent != "user" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid agent"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	if granularity != "daily" && granularity != "monthly" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid granularity"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if start, err = validateTimestamp(params.ByName("start")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid start timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	if end, err = validateTimestamp(params.ByName("end")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid end timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	ctx := context.Background()

	query := `SELECT spider, user, timestamp FROM "local_group_default_T_mediarequest_per_file".data WHERE "_domain" = 'analytics.wikimedia.org' AND referer = ? AND file_path = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`
	scanner := s.session.Query(query, referer, decodedFilePath, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var spider int
	var user int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&spider, &user, &timestamp); err != nil {
			s.logger.Error("Query failed: %s", err)
			problem.New(
				problem.Type("about:blank"),
				problem.Title(http.StatusText(http.StatusInternalServerError)),
				problem.Custom("method", http.MethodGet),
				problem.Status(http.StatusInternalServerError),
				problem.Detail(err.Error()),
				problem.Custom("uri", r.RequestURI)).WriteTo(w)
		}
		response.Items = append(response.Items, PerFile{
			Referer:     referer,
			FilePath:    decodedFilePath,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Requests:    filterAgent(agent, spider, user),
		})
	}

	if err := scanner.Err(); err != nil {
		s.logger.Error("Error querying database: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	w.Write(data)
}

func filterAgent(agent string, spider int, user int) int {
	if agent == "user" {
		return user
	} else if agent == "spider" {
		return spider
	} else {
		return spider + user
	}
}
