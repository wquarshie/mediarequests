package main

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

// TopResponse represents the API resultset	.
type TopResponse struct {
	Items []Top `json:"items"`
}

// Top represents an entry in the API resultset.
type Top struct {
	Referer   string     `json:"referer"`
	MediaType string     `json:"media_type"`
	Year      string     `json:"year"`
	Month     string     `json:"month"`
	Day       string     `json:"day"`
	Files     []TopFiles `json:"top"`
}

// TopFiles represents the set of files returned within the API resultset.
type TopFiles struct {
	FilePath string `json:"file_path"`
	Requests int    `json:"requests"`
	Rank     int    `json:"rank"`
}

// TopHandler is an HTTP handler for mediarequests/top API requests.
type TopHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *TopHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var filesData = []TopFiles{}
	var response = TopResponse{Items: make([]Top, 0)}

	// Parameters
	var referer = strings.ToLower(params.ByName("referer"))
	var mediaType = strings.ToLower(params.ByName("media-type"))
	var year = params.ByName("year")
	var month = params.ByName("month")
	var day = params.ByName("day")

	ctx := context.Background()

	var filesJSON string

	query := `SELECT "filesJSON" FROM "local_group_default_T_mediarequest_top_files".data WHERE "_domain" = 'analytics.wikimedia.org' AND referer = ? AND media_type = ? AND year = ? AND month = ? AND day = ?`
	if err := s.session.Query(query, referer, mediaType, year, month, day).WithContext(ctx).Consistency(gocql.One).Scan(&filesJSON); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Query failed; %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if err = json.Unmarshal([]byte(filesJSON), &filesData); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	var files = []TopFiles{}

	for _, s := range filesData {
		files = append(files, TopFiles{
			FilePath: s.FilePath,
			Requests: s.Requests,
			Rank:     s.Rank,
		})
	}

	response.Items = append(response.Items, Top{
		Referer:   referer,
		MediaType: mediaType,
		Year:      year,
		Month:     month,
		Day:       day,
		Files:     files,
	})

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	w.Write(data)
}
