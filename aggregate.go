package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

// AggregateResponse represents the API resultset	.
type AggregateResponse struct {
	Items []Aggregate `json:"items"`
}

// Aggregate represents an entry in the API resultset.
type Aggregate struct {
	Referer     string `json:"referer"`
	MediaType   string `json:"media_type"`
	Agent       string `json:"agent"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Requests    int    `json:"requests"`
}

// AggregateHandler is an HTTP handler for mediarequests/aggregate API requests.
type AggregateHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *AggregateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var response = AggregateResponse{Items: make([]Aggregate, 0)}

	// Parameters
	var referer = strings.ToLower(params.ByName("referer"))
	var mediaType = strings.ToLower(params.ByName("media-type"))
	var agent = params.ByName("agent")
	var granularity = strings.ToLower(params.ByName("granularity"))
	var start, end string

	// Parameter validation
	if granularity != "daily" && granularity != "monthly" {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid granularity"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if start, err = validateTimestamp(params.ByName("start")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid start timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	if end, err = validateTimestamp(params.ByName("end")); err != nil {
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail("Invalid end timestamp"),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	ctx := context.Background()

	query := `SELECT requests, timestamp FROM "local_group_default_T_mediarequest_per_referer".data WHERE "_domain" = 'analytics.wikimedia.org' AND referer = ? AND media_type = ? AND agent = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`
	scanner := s.session.Query(query, referer, mediaType, agent, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var requests int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&requests, &timestamp); err != nil {
			s.logger.Error("Query failed: %s", err)
			problem.New(
				problem.Type("about:blank"),
				problem.Title(http.StatusText(http.StatusInternalServerError)),
				problem.Custom("method", http.MethodGet),
				problem.Status(http.StatusInternalServerError),
				problem.Detail(err.Error()),
				problem.Custom("uri", r.RequestURI)).WriteTo(w)
		}
		response.Items = append(response.Items, Aggregate{
			Referer:     referer,
			MediaType:   mediaType,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Requests:    requests,
		})
	}

	if err := scanner.Err(); err != nil {
		s.logger.Error("Error querying database: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Error("Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	w.Write(data)
}

func validateTimestamp(param string) (string, error) {
	var err error
	var timestamp string

	if len(param) == 8 {
		timestamp = fmt.Sprintf("%s00", param)
	} else {
		timestamp = param
	}

	if _, err = time.Parse("2006010203", timestamp); err != nil {
		return "", err
	}

	return timestamp, nil
}
